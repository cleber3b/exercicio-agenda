package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {
    private String nome;
    private String email;
    private Integer telefone;

    public Pessoa(String nome, String email, Integer telefone) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
    }

    public Pessoa(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

}
