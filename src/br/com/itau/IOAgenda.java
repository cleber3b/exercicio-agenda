package br.com.itau;

import java.util.*;

public class IOAgenda {

    public static void imprimirMensagemInicial() {
        System.out.println("**********************");
        System.out.println("Bem Vindo ao Sistema de Agenda!!!");
        System.out.println("**********************");
    }

    public static Pessoa imprimirSolicitaDadosPessoa() {

        Scanner scanner = new Scanner(System.in);
        Pessoa pessoa = new Pessoa();

        System.out.println("**********************");
        System.out.println("Digite os dados da Pessoa");
        System.out.println("**********************");
        System.out.print("Nome: ");
        pessoa.setNome(scanner.nextLine());

        System.out.print("E-mail: ");
        pessoa.setEmail(scanner.nextLine());

        System.out.print("Telefone: ");
        pessoa.setTelefone(scanner.nextInt());

        return pessoa;
    }

    public static Pessoa imprimirRemovePessoa() {

        Scanner scanner = new Scanner(System.in);
        Pessoa pessoa = new Pessoa();

        System.out.println("**********************");
        System.out.println("Remover Contato da Agenda");
        System.out.println("**********************");
        System.out.println("Digite o E-mail para remover: ");
        pessoa.setEmail(scanner.nextLine());

        return pessoa;
    }

    public static Pessoa imprimirPesquisaContato() {

        Scanner scanner = new Scanner(System.in);
        Pessoa retornoPessoa = new Pessoa();

        System.out.println("**********************");
        System.out.println("Pesquisar Contato da Agenda");
        System.out.println("**********************");
        System.out.println("Escolha uma Opção");
        System.out.println(" [1] - pesquisar por Email");
        System.out.println(" [2] - pesquisar por Número de Telefone (somente números)");
        System.out.print("Digite: ");
        String opcao = scanner.nextLine();

        if (opcao == "1") {
            System.out.print("Email: ");
            retornoPessoa.setEmail(scanner.nextLine());
        } else if (opcao == "2") {
            System.out.print("Telefone: ");
            retornoPessoa.setTelefone(scanner.nextInt());
        }

        return retornoPessoa;
    }

    public static void imprimirContatoPesquisado(Pessoa pessoa) {

        System.out.println("***********************************");
        System.out.println("Nome: " + pessoa.getNome());
        System.out.println("Email: " + pessoa.getEmail());
        System.out.println("Telefone: " + pessoa.getTelefone());
        System.out.println("***********************************");

    }

    public static void imprimirContatoRemovido(Pessoa pessoa) {

        System.out.println("***********************************");
        System.out.println("Nome: " + pessoa.getNome());
        System.out.println("Email: " + pessoa.getEmail());
        System.out.println("Telefone: " + pessoa.getTelefone());
        System.out.println("***********************************");

    }

    public static int solicitarQuantidadePessoasIncluirAgenda() {

        Scanner scanner = new Scanner(System.in);

        System.out.println("**********************");
        System.out.println("Quantas Pessoas deseja incluir na Agenda?");
        System.out.println("**********************");
        System.out.print("Quantidade: ");
        int qtdContatosAdicionar = scanner.nextInt();

        return qtdContatosAdicionar;
    }

    public static void imprimirMensagemFinalSistema() {
        System.out.println("*************************************");
        System.out.println("Obrigado por usar o sistema de Agenda");
        System.out.println("*************************************");
    }

}
