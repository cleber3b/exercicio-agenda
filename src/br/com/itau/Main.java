package br.com.itau;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        IOAgenda.imprimirMensagemInicial();

        Agenda agenda = new Agenda();
        Pessoa pessoa = new Pessoa();

        /*Seta quantidade de pessoa a serem incluidas*/
        agenda.setQtdContatosAdicionar(IOAgenda.solicitarQuantidadePessoasIncluirAgenda());

        /*Inclui pessoas de acordo com a quantidade informada*/
        if (agenda.getQtdContatosAdicionar() != 0) {
            for (int i = 0; i < agenda.getQtdContatosAdicionar(); i++) {

                pessoa = IOAgenda.imprimirSolicitaDadosPessoa();
                agenda.IncluirContato(pessoa);
            }
        } else {
            IOAgenda.imprimirMensagemFinalSistema();
        }

        /*Remove contato*/
        Pessoa removePessoa = new Pessoa();
        removePessoa = IOAgenda.imprimirRemovePessoa();
        agenda.RemoverContato(removePessoa);

        /*Pesquisando um Contato*/
        Pessoa pesquisaPessoa = new Pessoa();
        pesquisaPessoa = IOAgenda.imprimirPesquisaContato();
        pesquisaPessoa = agenda.PesquisaContato(pesquisaPessoa);
        IOAgenda.imprimirContatoPesquisado(pesquisaPessoa);

        IOAgenda.imprimirMensagemFinalSistema();
    }
}
