package br.com.itau;

import java.util.List;

public interface Contato {

    void RemoverContato(Pessoa pessoa);

    Pessoa PesquisaContato(Pessoa pessoa);

    void IncluirContato(Pessoa pessoa);

}
