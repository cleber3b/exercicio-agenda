package br.com.itau;

import com.sun.xml.internal.ws.api.model.ExceptionType;
import sun.awt.image.IntegerInterleavedRaster;

import java.util.*;

public class Agenda implements Contato {
    List<Pessoa> listaContato = new ArrayList<>();
    private Integer qtdContatosAdicionar;

    public Agenda(List<Pessoa> listaContato) {
        this.listaContato = listaContato;
    }

    public Agenda() {

    }

    public Integer getQtdContatosAdicionar() {
        return qtdContatosAdicionar;
    }

    public void setQtdContatosAdicionar(Integer qtdContatosAdicionar) {
        this.qtdContatosAdicionar = qtdContatosAdicionar;
    }

    @Override
    public void RemoverContato(Pessoa pessoa) {
        for (Pessoa pes : this.listaContato) {
            if (pes.getEmail().equals(pessoa.getEmail())) {
                this.listaContato.remove(pes);
            }
        }
    }

    @Override
    public Pessoa PesquisaContato(Pessoa pessoa) {

        Pessoa pessoaPesquisada = new Pessoa();

        for (Pessoa pesPesq : this.listaContato) {
            if (pesPesq.getEmail().equals(pessoa.getEmail())) {

                pessoaPesquisada.setNome(pesPesq.getNome());
                pessoaPesquisada.setEmail(pesPesq.getEmail());
                pessoaPesquisada.setTelefone(pesPesq.getTelefone());
            } else if (pesPesq.getTelefone().equals(pessoa.getTelefone())) {
                pessoaPesquisada.setNome(pesPesq.getNome());
                pessoaPesquisada.setEmail(pesPesq.getEmail());
                pessoaPesquisada.setTelefone(pesPesq.getTelefone());
            }
        }

        return pessoaPesquisada;
    }

    @Override
    public void IncluirContato(Pessoa pessoa) {
        this.listaContato.add(pessoa);
    }
}
